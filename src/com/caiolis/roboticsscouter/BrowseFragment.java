package com.caiolis.roboticsscouter;

import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v4.app.ListFragment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class BrowseFragment extends ListFragment {
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
		getActivity().setTitle("Browse Robots");
		ArrayAdapter<Robot> adapter = new ArrayAdapter<Robot>(getActivity(), android.R.layout.simple_list_item_1, Robot.getRobots());
		setListAdapter(adapter);
	}
	
	public void onResume() {
		super.onResume();
		((BaseAdapter) getListAdapter()).notifyDataSetChanged();
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_browse_menu, menu);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onOptionsItemSelected(android.view.MenuItem)
	 * 
	 * Starts an AddActivity which hosts an AddFragment. I will perhaps work on switching out the fragments
	 * at a much later point in time.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case  R.id.menu_add_robot:
				// This tried to swap in a fragment, but I'm just going to create an AddActivity
				/*
				Log.d("BrowseFragment", "Before launching a fragment");
				getActivity().setContentView(R.layout.fragment_add);
				Log.d("BrowseFragment", "After setting content view");
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new AddFragment()).commit();
				*/
				startActivity(new Intent(getActivity(), AddActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public void onListItemClick(ListView l, View v, int position, long id) {
		Robot robot = (Robot)getListAdapter().getItem(position);
		Intent intent = new Intent(getActivity(), ViewActivity.class);
		intent.putExtra(Robot.ROBOT, robot);
		startActivity(intent);
	}
	
}
