package com.caiolis.roboticsscouter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainFragment extends Fragment{
	
	private Button add;
	private Button browse;
	private Button send;
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 * 
	 * This guy doesn't do anything right now, but it's here if I will need it later.
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * 
	 * Grabs the buttons as state.
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		add = (Button)rootView.findViewById(R.id.add_robot_button);
		browse = (Button)rootView.findViewById(R.id.browse_button);
		send = (Button)rootView.findViewById(R.id.send_robot_button);
		add.setOnClickListener(new AddClickListener());
		browse.setOnClickListener(new BrowseClickListener());
		send.setOnClickListener(new SendClickListener());
		return rootView;
	}
	
	private class AddClickListener implements OnClickListener {
		public void onClick(View v) {
			startActivity(new Intent(getActivity(), AddActivity.class));
		}
	}
	
	private class BrowseClickListener implements OnClickListener {
		 public void onClick(View v) {
			 startActivity(new Intent(getActivity(), BrowseActivity.class));
		 }
	}
	
	private class SendClickListener implements OnClickListener {
		public void onClick(View v) {
			// Switch to send FRAGMENT
		}
	}
}
