package com.caiolis.roboticsscouter;

import java.util.ArrayList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class ViewFragment extends ListFragment {
	
	//private TextView name;
	private Robot robot;
	
	/*
	 * There will be a little bit more to this class once the edit robot button in the action bar comes into 
	 * play, so make sure that things get changed accordingly.
	 */
	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		// setHasOptionsMenu(true); I want there to be an edit robot button!!!
		robot = (Robot)getActivity().getIntent().getSerializableExtra(Robot.ROBOT);
		getActivity().setTitle(robot.toString());
		RobotAdapter adapter = new RobotAdapter(robot.getAttributeNames());
		setListAdapter(adapter);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onPause()
	 * This method probably won't be needed for now... once there is an edit button, we will have to change
	 * that, so for now it will remain in the comments.
	 */
	/*
	public void onPause() {
		int robotAttributesLength = robot.getAttributes().size();
		for (int itemsToRemove = data.size() - robotAttributesLength; itemsToRemove > 0; --itemsToRemove)
			data.remove(0);
		if (robotAttributesLength != 0)
			robot.clearAttributes();
		for (int i = 0; i < robotAttributesLength; ++i)
			robot.addAttribute(data.get(i).getText().toString());
	}
	*/
	
	/*
	 * this method WILL be important when there is an edit button in play...
	public void onResume() {
		super.onResume();
		((BaseAdapter) getListAdapter()).notifyDataSetChanged();
	}
	*/
	
	/*
	 * For the edit button. Change the code inside...
	 */
	/*
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_browse_menu, menu);
	}
	*/
	
	/* This method will look different, however it will start an AddFragment, most likely, which will edit the
	 * stuff inside of the robot.
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case  R.id.menu_add_robot:
				startActivity(new Intent(getActivity(), AddActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	*/
	
	
	/* this is for implementation RIGHT AWAY. this is not actual code for this class.
	
	public void onListItemClick(ListView l, View v, int position, long id) {
		Robot robot = (Robot)getListAdapter().getItem(position);
		Intent intent = new Intent(getActivity(), ViewActivity.class);
		intent.putExtra(Robot.ROBOT, robot);
		startActivity(intent);
	}
	*/
	
	private class RobotAdapter extends ArrayAdapter<String> {
		public RobotAdapter(ArrayList<String> attributes) {
			super(getActivity(), 0, attributes);
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			/*
			 * So, this line inflates list_itme_add... hopefully I will be able to change the EditText
			 * to undeditable so that everything works... 
			 */
			if (convertView == null)
				convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_add, null);
			String attribute = getItem(position);
			TextView title = (TextView)convertView.findViewById(R.id.addTitle);
			title.setText(attribute);
			EditText textfield = (EditText)convertView.findViewById(R.id.addTextfield);
			textfield.setKeyListener(null); // If it's not listening to keys... then nothing is changing!
			textfield.setText(robot.getAttributes().get(position));
			/*
			 * There may be some more here when the edit button in the action bar comes around... stay tuned for
			 * more!!
			 */
			return convertView;
		}
		
	}
	
}
