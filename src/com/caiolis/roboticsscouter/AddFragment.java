package com.caiolis.roboticsscouter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class AddFragment extends ListFragment {
	
	/*
	 * Something is screwy here :/
	 * 
	 */
	private Robot robot;
	private ArrayList<EditText> data = new ArrayList<EditText>();
	
	/*
	 * the only way to test this now is by making a "choose new field" dialog which appends a new list item 
	 * to the Add Fragment
	 */
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 * this makes sure that the instance is retained on something like a rotate as well as has an options menu.
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
		getActivity().setTitle("Add a new Robot");
		robot = new Robot();
		RobotAdapter adapter = new RobotAdapter(robot.getAttributeNames());
		setListAdapter(adapter);
		// getListView().getAdapter().getItem(0); -> returns an object
		// getListView().getAdapter().getCount();
	}
	
	public void onPause() {
		/*
		 * Make sure that the method that adds a new attribute field adds the attribute to the robot, too
		 */
		int robotAttributesLength = robot.getAttributes().size();
		for (int i = 0; i < robotAttributesLength; ++i)
			robot.setAttribute(i, data.get(i).getText().toString());
		((BaseAdapter)getListAdapter()).notifyDataSetChanged();
		super.onPause();
	}
	
	public void onDestroy() {
		/*
		for (EditText et : data)
			Log.d("Edit Text", et.getText().toString());
		ArrayList<Robot> robots = Robot.getRobots();
		for (Robot r : robots)
			Log.d("Robot", r.toString());
		ArrayList<String> attributeNames = robot.getAttributeNames();
		for (String s : attributeNames) 
			Log.d("Attribute Names", s);
		*/
		/*
		String name = robot.getAttributes().get(0);
		robot.setName(name);
		if (name.equals(""))
			Robot.removeRobot(robot.getUUID());
		*/
		super.onDestroy();
	}
	
	
	/*
	public void onDestroyView() {
		if (robot.equals(null)) {
			super.onDestroyView();
			return;
		}
		View v = getView();
		String attributeName = (String)((TextView)v.findViewById(R.id.addTitle)).getText();
		int numAttributeNames = robot.getAttributeNames().size();
		int i = 0;
		boolean attributeFound = false;
		while (i < numAttributeNames && !attributeFound) {
			if (attributeName.equals(robot.getAttributeNames().get(i++))) {
				attributeFound = true;
				--i;
			}
		}
		String attribute = (String)((TextView)v.findViewById(R.id.addTextfield)).getText();
		robot.setAttribute(i, attribute);
		super.onDestroyView();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (robot.equals(null))
			return super.onCreateView(inflater, container, savedInstanceState);
		View v = super.onCreateView(inflater, container, savedInstanceState);
		String attributeName = (String)((TextView)v.findViewById(R.id.addTitle)).getText();
		int numAttributeNames = robot.getAttributeNames().size();
		int i = 0;
		boolean attributeFound = false;
		while (i < numAttributeNames && !attributeFound) {
			if (attributeName.equals(robot.getAttributeNames().get(i++))) {
				attributeFound = true;
				--i;
			}
		}
		((TextView)v.findViewById(R.id.addTextfield)).setText(robot.getAttributes().get(i));
		return v;
	}
	*/
	
	// the options menu will have an "add field button" to insert more information
	// We can reuse the fragment_browse_menu xml because I really don't want it to look any different
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_browse_menu, menu);
	}
	
	/* This method is going to look completely different, this is just framework for right now.
	 * It will "listen" to another R.id.menu_something_something and it won't start another activity or anything
	 * it will probably start a dialog.
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case  R.id.menu_add_robot:
				startActivity(new Intent(getActivity(), AddActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	*/
	
	private class RobotAdapter extends ArrayAdapter<String> {
		public RobotAdapter(ArrayList<String> attributes) {
			super(getActivity(), 0, attributes);
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null)
				convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_add, null);
			String attribute = getItem(position);
			final TextView title = (TextView)convertView.findViewById(R.id.addTitle);
			title.setText(attribute);
			final EditText textfield = (EditText)convertView.findViewById(R.id.addTextfield);
			textfield.setOnKeyListener(new View.OnKeyListener() {

				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					String attributeName = (String)title.getText();
					ArrayList<String> attributeNames = robot.getAttributeNames();
					int numAttributeNames = attributeNames.size();
					int i = 0;
					boolean attributeFound = false;
					while (i < numAttributeNames && !attributeFound) {
						if (attributeName.equals(attributeNames.get(i++))) {
							attributeFound = true;
							--i;
						}
					}
					robot.setAttribute(i, textfield.getText().toString());
					return false;
				}
				
			});
			textfield.setText(robot.getAttributes().get(position));
			if (data.size() < position + 1)
				data.add(textfield);
			else
				data.set(position, textfield);
			return convertView;
		}
	}
}
