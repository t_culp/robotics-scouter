package com.caiolis.roboticsscouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class Robot implements Serializable {
	
	/*
	 * Have state of robot and lots of different stuffs.
	 */
	
	private static final long serialVersionUID = 1462235541372641748L;
	public final static String ROBOT = "robot";
	private static ArrayList<Robot> robots = new ArrayList<Robot>();
	private ArrayList<String> attributeNames = new ArrayList<String>();
	private ArrayList<String> attributes = new ArrayList<String>();
	
	private UUID id;
	private String name = "";
	
	/*
	 * Finish making an enum which will hold preset attributes of a robot. Make sure to fix the fact that 
	 * the ArrayAdapter in AddFragment will be going through each robot. Make it go through THIS robot's array
	 * list for stuff.
	 */
	private enum presetAttribute {
		NAME       ("Name of robot/team"), 
		LOCATION   ("Where is the team from?"), 
		EVENT      ("Name of event robot was met at"), 
		BUILD      ("Stacks Skyrise sections?"), 
		NUMOFCUBES ("Number of cubes robot can carry"),
		ROBOTOS    ("Programming Language (Robot OS)");
		
		private final String title;
		
		private presetAttribute(String title) {
			this.title = title;
		} // close constructor
		
		private String title() {
			return title;
		} // close title
		
	} // close serverCommand
	
	public Robot() {
		id = UUID.randomUUID();
		robots.add(this);
		for (presetAttribute p : presetAttribute.values()) {
			attributeNames.add(p.title());
			attributes.add("");
		}
	}
	
	
	public static ArrayList<Robot> getRobots() {
		if (robots.size() == 0) {
			for (int i = 0; i < 40; ++i) {
				(new Robot()).setName("Robot #" + (i + 1));
			}
		}
		return robots;
	}
	
	public static void removeRobot(UUID robotUUID) {
		boolean robotFound = false;
		int i = 0;
		int length = robots.size();
		while (i < length && !robotFound) {
			if (robotUUID.equals(robots.get(i++).getUUID())) {
				robots.remove(i - 1);
				robotFound = true;
			}
		}
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAttribute(int index, String attribute) {
		attributes.set(index, attribute);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * Since toString returns the name, there is no need for a getName() getter
	 */
	public String toString() {
		return name;
	}
	
	public UUID getUUID() {
		return id;
	}
	
	public void addAttributeName(String name) {
		attributeNames.add(name);
	}
	
	public void addAttribute(String attribute) {
		attributes.add(attribute);
	}
	
	public ArrayList<String> getAttributeNames() {
		return attributeNames;
	}
	
	public ArrayList<String> getAttributes() {
		return attributes;
	}
	
	public void clearAttributes() {
		attributes.clear();
	}
	
}
