Robotics Scouter is an Android application which aids in scouting at robotics
competitions. This app helps collect information in the form of fields pertinent
to current competitions and organize it in an efficient manner. It also yields
the ability to share information collected on robots in formats that can be
read as well as interpreted by this application.